
#
# You can add a section to your ~/.config/fish/config.fish file that will set
# your path correctly on login. This is much like .profile or .bash_profile as it
# is only executed for login shells.
#
if status --is-login
        set PATH  /Users/cs/bin /Users/cs/opt/bin \
            /Users/cs/Dropbox/bin \
            /Applications/Xcode.app/Contents/Developer/usr/bin \
            /usr/local/share/npm/bin \
            /usr/local/sbin \
            /use/local/bin  $PATH

            source ~/.config/fish/aliases.fish
            source ~/.config/fish/plugins/brew/brew.load
end

set -x EDITOR "vim"
set -x CLICOLOR 1
set -x BROWSER open
set fish_greeting (date)



