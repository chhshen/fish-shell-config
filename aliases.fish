echo "loading aliases.fish"

alias d "cd ~/Desktop"
alias l "ls -lFh"
alias cp "cp -i"
alias mv "mv -i"


function ll
    ls -lFh $argv
end

function ..
    builtin cd ..
end

function ...
    builtin cd ../..
end


                            # alias for `open'
function o

    if test (count $argv) -gt 0
        set filename $argv[1]
    else
        open .; and return 0
    end

    switch $filename
    case '*.pdf'
        open -askim "$filename"; and return 0
    end

    open "$argv"

end



function rm -d "Remove files/dirs into ~/.tmp!"

    if test (count $argv) = 0
        echo "What do you want to delete?"
        return 1
    end

    if test -L $argv[1]
        echo "Unlink symbolic link: $argv[1]."
        unlink $argv[1]
        return 0
    end

    if command_exists rmtrash
        echo "moving file(s) to the trash ..."
        rmtrash $argv
    else
        echo "No rmtrash installed! Try /bin/rm."
        echo "No files deleted."
    end
end


function pgrep
    ps aux | grep $argv | grep -v grep
end


