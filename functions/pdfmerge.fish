function pdfmerge

    if not command_exists pdftk
      echo "pdftk not found!"
      return 1
    end

    set -l curtime (date | sed s/" "/"-"/g | sed s/:/"_"/g)

    pdftk $argv cat output CombinedPDF_$curtime.pdf

    echo 'The merged PDF is CombinedPDF_'$curtime'.pdf'

end

