# test if a command exists
function command_exists

    if test (count $argv) = 0
        echo "One input argument needed."
        return 1
    end

    set command_name $argv[1]

    if type "$command_name" > /dev/null
        set cmd_loc (type -p "$command_name")

        if [ -z $cmd_loc ]
            return 1
        end

    else
        return 1
    end

    return 0
end


