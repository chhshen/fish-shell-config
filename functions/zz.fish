
function zz -d "zip a file/directory."

     if [ (count $argv) = 0 ]
             echo "you need to provide a file or dir to zip."
             return 1
     end

     set DIR      "$argv[1]"
     set filename (basename $DIR)
     set DATE     (date +%m%d-%H%M)

     set f  "$filename""-"$DATE.zip
     echo "... Compressing files into:  ""$f"
     zip -r "$f"  $DIR

 end

