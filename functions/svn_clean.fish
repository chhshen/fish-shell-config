
function svn_clean -d  "remove LaTeX auxiliary files from a svn repo"

  for ext in "*bak" "*log" "*swp" "*~" "*bbl" "*blg" "*aux" "*dvi" \
        "*synctex.gz" "*sav" "*fmt" "*out" "*fdb_latexmk" "*fls"

        echo "svn deleting:  "$ext
        find . -type f -name "$ext" \
               | xargs svn delete --force
  end

end

