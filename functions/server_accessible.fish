
function server_accessible

  if test (count $argv) = 0
        echo "One input argument needed."
        return 1
  end

  set -l SRV $argv[1]

  ping -W1 -c1 -q $SRV ^/dev/null >/dev/null

  set -l srv_status $status

  return $srv_status

end


