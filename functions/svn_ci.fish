function svn_ci  -d "wrap of svn ci -m ...."
    set -l model (system_profiler SPHardwareDataType | grep "Model Name" | awk '{ print $3 $4 $5 }')
    set -l location "Unknown location"

    ifconfig | grep inet | grep 192.168.0  > /dev/null

    if test $status
      set -l location Home
    end

    ifconfig | grep inet | grep 129.127  > /dev/null
    if test $status
        set -l location Office
    end

    set -l machine "OSX@"$model", from "$location
    set -l user (whoami | tr '[:lower:]' '[:upper:]')

    set -l curp (basename (pwd))

    set  msg " @"(date '+%d/%m/%Y %H:%M:%S')" by $user, $machine:$curp"

    if test (count $argv) -gt 0
      set  msg "NOTE:""$argv"$msg
    end

    echo "check-in message: ""$msg"
    svn ci -m"$msg"

    set -e msg
end

