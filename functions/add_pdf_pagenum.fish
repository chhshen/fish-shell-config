
function add_pdf_pagenum -d "Fish script for adding page numbers to a PDF using PDFTK"

    set -l PDFDIR PDF_(date | sed -e 's/ /_/g' | sed -e 's/:/_/g')
    set -l PWD1 (pwd)

    if test (count $argv) -lt 1
      echo "Please tell me the input PDF file!"
      return 1
    end

    set -l INPUTPDF0 "$argv[1]"
    echo "Input PDF file is: "$INPUTPDF0

    if not test -f "$INPUTPDF0"
      echo "Input PDF file not found! Please double check!"
      return 2
    end


    echo "Checking for dependencies"
    if not command_exists  pdftk
      echo "Dependencies not met: pdftk not found"
      echo "Please install pdftk and try again"
      return 3
   end

    if not command_exists  pdflatex
      echo "Dependencies not met: pdflatex not found"
      echo "Please install pdflatex and try again"
      return 3
    end

    echo "Preparing ..."


    mkdir -p /tmp/$PDFDIR
    cp -f $INPUTPDF0  /tmp/$PDFDIR
    set -l INPUTPDF (basename $INPUTPDF0)

    cd /tmp/$PDFDIR
    echo "Calculating Page Numbers ..."
    #If you want to have some pages to not be numbered simply have this script move them to ./temp at this point

    set -l PAGE (pdfinfo $INPUTPDF |  grep -i "Pages" | awk '{print $2}' )

    echo "Creating Page Number file for "$PAGE" pages"

    set -l tfile "Numbers.tex"


    echo   '\\documentclass[10pt]{article}'   > $tfile
    echo   '\\usepackage{multido,fancyhdr}'   >> $tfile
    echo   '\\usepackage[hmargin=0cm,vmargin=1.5cm,nohead,nofoot]{geometry}' >> $tfile
    echo   '\\pagestyle{fancy}'  >> $tfile
    echo   '\\fancyhead{}'       >> $tfile
    echo     '\\fancyfoot{}'     >> $tfile
    echo     '\\renewcommand{\\headrulewidth}{0pt}'   >> $tfile
    echo     '\\renewcommand{\\footrulewidth}{0pt}' >> $tfile
    echo     '\\rfoot{$\cdot$ \it\\thepage$/$'$PAGE' $\cdot$ \\hspace{1cm} }'  >> $tfile
    echo     '\\begin{document}'                                               >> $tfile
    echo     '\\multido{}{'$PAGE'}{\\vphantom{x}\\newpage}'                    >> $tfile
    echo     '\\end{document}'  >> $tfile


    pdflatex -interaction=batchmode numbers.tex 1>/dev/null

    echo "Bursting PDF's"
    pdftk $INPUTPDF   burst output prenumb_burst_%03d.pdf
    pdftk numbers.pdf burst output number_burst_%03d.pdf

    echo "Now adding Page Numbers:"

    for  i in (seq 1 $PAGE)
      set  iii $i
      set  length (echo $i | wc -c)
      set length (math $length -1 )

      if test $length -eq 1
         set  iii 00$i
      end

      if test $length -eq 2
         set iii 0$i
      end

      pdftk prenumb_burst_$iii.pdf background number_burst_$iii.pdf \
        output ./numbered_tomerge_$iii.pdf
   end

  echo "Merging *.pdf files"
  set -l PDF2Merge (ls -1 ./numbered_tomerge_*.pdf)

  pdftk $PDF2Merge cat output ./numbered.pdf

  echo "Optimizing PDF file"

    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer \
          -dNOPAUSE \-dQUIET -dBATCH -sOutputFile=numbered_bloat.pdf  numbered.pdf 2>/dev/null


  set -l OUTPDF "$PWD1"/(basename $INPUTPDF0  .pdf)"_num.pdf"
  cp -i ./numbered_bloat.pdf $OUTPDF

  echo "Cleaning Up"

  cd $PWD1
  /bin/rm -fr  /tmp/$PDFDIR
  echo "The PDF file with page numbers is: $OUTPDF"

  set -e iii
  set -e length

end



